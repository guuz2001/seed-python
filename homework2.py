
#7/13 HOMEWORK: finish this stuff man

#write function to compute factorial
"""
def factorial(n):
    runningProduct = 1
    for i in range(1,n+1):
        runningProduct = runningProduct * i
    return runningProduct

print("gimme an integer")
number = int(input())
print(factorial(number))
"""

#write function for exp(n) or 2 to the power of n
"""
def exp(n):
    product = 2**n
    return product

print("gimme a number")
number = float(input())
print(exp(number))
"""

#function to compute n!/2^n
"""
def factorial(n):
    runningProduct = 1
    for i in range(1,n+1):
        runningProduct = runningProduct * i
    return runningProduct

def exp(m):
    product = 2**m
    return product

def complicated(q):
    thing1 = factorial(q)
    thing2 = exp(q)
    thing3 = thing1/thing2
    return thing3

print("gimme an integer")
number = int(input())
print(complicated(number))
"""

#function for triangle(n) or N + (n-1) + (n-2) + wjhwfjhflsjg
"""
def triangle(n):
    runningSum = 0
    for i in range(1,n+1):
        runningSum += i
    return runningSum

print("give integer")
number = int(input())
print(triangle(number))
"""

#write function for cube_n(n) or n^3 + (n-1)^3 + ... + 1^3
"""
def cube_n(n):
    runningSum = 0
    for i in range(1,n+1):
        runningSum += i**3
    return runningSum

print("give integer")
number = int(input())
print(cube_n(number))
"""

#function triangle_sq(n) or (n + (n-1) + (n-2) + weuifdsf)^2
"""
def triangle_sq(n):
    runningSum = 0
    for i in range(1,n+1):
        runningSum += i
    return runningSum**2

print("give integer")
number = int(input())
print(triangle_sq(number))
"""

#check if cube_n(n) == triange_sq(n) for n=1 to 100
"""
def cube_n(n):
    runningSum = 0
    for i in range(1,n+1):
        runningSum += i**3
    return runningSum

def triangle_sq(n):
    runningSum = 0
    for i in range(1,n+1):
        runningSum += i
    return runningSum**2

for i in range(1,101):
    if cube_n(i) == triangle_sq(i):
        print("equal at i = " + str(i))
#huh
"""

#write leibniz(n) or 4*(1-(1/3)+(1/5)-(1/7)+...) up to n terms
"""
def leibniz(n):
    runningSum = 0
    for i in range(1,n+1):
        if i % 2 == 0:
            sign = (-1)
        else:
            sign = 1
        runningSum += sign*(1/(2*i-1))
    return 4*(runningSum)

print("give integer")
n = int(input())
print(leibniz(n))
"""

#for what value of n does this series equal math.pi?
"""
import math

def leibniz(n):
    runningSum = 0
    for i in range(1,n+1):
        if i % 2 == 0:
            sign = (-1)
        else:
            sign = 1
        runningSum += sign*(1/(2*i-1))
    return 4*(runningSum)

for j in range(1000001, 1000000001):
    if leibniz(j) == math.pi:
        print("equal at j = " + str(j))
        break
    if j == 1000005:
        print(j)
    if j == 1000015:     #just to measure the pace of the program running
        print(j)

#??? should it be this big???
"""

#print out all pairs from 0 to 9 in increasing order only
#e.g. (0,0) (0,1) jfjshfadsf
#next line (1,0), (1,1)
"""
for i in range(10):
    for j in range(10):
        print("(" + str(i )+ ", " + str(j) + ")  ", end='')
        if j == 9:
            print()
"""

#write prime checking function again without references, try to improve speed/checking fewer numbers
"""
print("can i have intejer")
n = int(input())

for i in range(2,(n//2)+1):
    if (n%i) == 0:
        print("has factor of " + str(i), end='')
        for j in range(i+1,(n//2)+1):
            if (n%j) == 0:
                print(", " + str(j))
            else:
                print('')
#dunno how to have it check other numbers in advance before i and then avoid repetition once i naturally reaches those numbers in the range() function
"""
