# COLLATZ CONJECTURE
# For all integers n, the following process will terminate
# if n = 1: stop
# if n is even, divide by 2
# if n is odd, multiply by 3 and add 1
# User inputs a number n
# Given n, return number of steps before terminating
# Keep track of the maximal value during this process and print that as well
"""
print("gimme integer")
number = int(input())
stepCounter = 0
runningMax = 0

while True:
    if number > runningMax:
        runningMax = number
    if number == 1:
        print("youve reached one!")
        print("step count = " + str(stepCounter))
        print("maximum = " + str(runningMax))
        break
    elif number % 2 !=  0:
        number = 3 * number + 1
        stepCounter += 1
        continue
    elif number % 2 == 0:
        number = number / 2
        stepCounter += 1
        continue
"""

# FIBONACCI NUMBERS
# fib(n)
# Given n, output the nth Fibonacci number
# ex. n = 0: 0, n = 1: 1, n = 2: 1, n = 3: 2
"""
def fib(n):
    lastNumber = 0
    ohYeahNumber = 1
    if n == 0:
        return 0
    if n == 1:
        return 1
    if n >= 2:
        for i in range(2,n+1):
            ohYeahNumber = lastNumber + ohYeahNumber
            lastNumber = ohYeahNumber - lastNumber
            if i == n:
                return ohYeahNumber


print("gimme integer")
n = int(input())
print("fibonacci number " + str(n) + " is " + str(fib(n)))
"""

# BRUTE FORCE GCD:
# gcd(a, b)
# Given two numbers, compute their greatest common divisor
# Idea: think about the largest possible GCD and work your way down and check
# if your candidate divides both.
"""
print("give number")
n1 = int(input())
print("another (no repeats)")
n2 = int(input())

if n1 < n2:
    top = n1
else:
    top = n2

def gcd(n1,n2):
    for i in range(top,0,-1):
        if n1 % i == 0 and n2 % i == 0:
            return "GCD is " + str(i)
            break

print(gcd(n1,n2))
"""

# LUCAS SEQUENCE:
# lucas(p, q, n)
# Lucas sequence is a generalization of Fibonacci sequence
# L_0 = 0
# L_1 = 1
# L_n = p * L_{n-1} - q * L_{n-2}
# if p = 1 and q = -1, then this is just Fibonacci
# Write a function that takes in p,q,n and outputs nth Lucas number
"""
print("give integer (p)")
p = int(input())
print("give integer (q)")
q = int(input())
print("give integer (n)")
n = int(input())

def Lucas(p,q,n):
    L_0 = 0
    L_1 = 1
    if n == 0:
        return 0
    if n == 1:
        return 1
    if n >= 2:
        for i in range(2,n+1):
            L_1 = p * L_1 - q * L_0
            L_0 = (L_1 + q * L_0) / p
            if i == n:
                return L_1

print("term number " + str(n) + " of your Lucas sequence is " + str(Lucas(p,q,n)))
"""

# MAX GAP:
# max_gap(arr)
# Given an array of integers, find the largest "gap", meaning the
# largest absolute difference in value between any two consecutive elements.
# ex. [5,8,1,2,-5,3,9] => 8 (between -5 and 3)
"""
import math

print("enter as many integers (positive or negative) as you want")
print('enter "1234567890" when you are done')

arr = []

while True:
    news = int(input())
    if news == 1234567890:
        break
    arr.append(news)

def max_gap(arr):
    runningMax = -1 * math.inf
    runningMin = math.inf
    for number in arr:
        if number < runningMin:
            runningMin = number
        if number > runningMax:
            runningMax = number
    return runningMax - runningMin

print()
print("the max gap is " + str(max_gap(arr)))
"""

# PYTHON PARAGRAPH
# In different file
